import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipebomb/blocs.dart';
import 'package:recipebomb/repositories.dart';
import 'package:recipebomb/widgets.dart';
import 'package:recipebomb/screens.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => CategoryPage());
      case '/recipeList':
        // return MaterialPageRoute(
        //     builder: (_) => RecipeListPage(settings.arguments));
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) =>
                RecipeBloc(recipeRepository: RecipeRepository()),
            child: RecipeListPage(settings.arguments),
          ),
        );
      case '/recipe':
        Arguments args = settings.arguments;
        // return MaterialPageRoute(
        //     builder: (_) => RecipePage(
        //           categoryName: args.categoryName,
        //           recipe: args.recipe,
        //         ));
        return MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) =>
                RecipeBloc(recipeRepository: RecipeRepository()),
            child: RecipePage(
              categoryName: args.categoryName,
              recipe: args.recipe,
            ),
          ),
        );
      case '/myFridge':
        return MaterialPageRoute(builder: (_) => MyFridgePage());
      case '/addition':
        return MaterialPageRoute(
            builder: (_) => BlocProvider(
                  create: (context) =>
                      RecipeBloc(recipeRepository: RecipeRepository()),
                  child: AdditionPage(),
                ));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
