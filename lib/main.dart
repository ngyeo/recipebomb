import 'package:flutter/material.dart';
import 'package:recipebomb/models.dart';
import 'package:recipebomb/routes.dart' as r;
import 'package:recipebomb/screens.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  List<String> vegetables = [
    'Garlic',
    'Onion',
    'Zucchini',
    'Mushroom',
    'Pumpkin',
    'Eggplant',
    'Cabbage',
    'Lettuce'
  ];
  List<String> sauce = [
    // 'Olive oil',
    'Soy sauce',
    // 'Ketchup',
    'Mayonnaise',
  ];

  // Fridge().initialize(List.generate(vegetables.length,
  //     (i) => Ingredient(name: vegetables[i], type: 'Vegetables')));
  // Fridge().initialize(List.generate(
  //     sauce.length, (i) => Ingredient(name: sauce[i], type: 'Sauce')));
  // await Fridge().getAllState();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Color.fromRGBO(0, 70, 42, 1.0),
        // primaryColor: Color(0xff00462A),
        buttonColor: Colors.white,
      ),
      onGenerateRoute: r.Router.generateRoute,
      // home: CategoryPage(),
      home: BootApp(),
    );
  }
}

class BootApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: setIngredients(),
        builder: (context, _) => CategoryPage(),
      ),
    );
  }

  Future<void> setIngredients() async {
    QuerySnapshot snapshot = await Firestore.instance.collection('ingredient').getDocuments();
    Fridge().initialize(List<Ingredient>.from(snapshot.documents.map<Ingredient>((ingrd) => Ingredient.fromMap(ingrd))));
    await Fridge().getAllState();
    print(Fridge.ingredientsMap);
  }
}

const MaterialColor ewhaGreen = const MaterialColor(0xFF00462A, const <int, Color>{});
