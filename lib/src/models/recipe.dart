import 'package:flutter/material.dart';

class Recipe {
  /// Category name of this recipe.
  ///
  /// This may be changed to Category type.
  final String categoryName;

  /// Unique code of this recipe.
  ///
  /// This is filled when we get data, not insertion.
  final String code;

  /// The name of this recipe.
  final String name;

  /// Ingredients as a form of <name, unit>
  /// ```
  /// <'greenbeans', '100g'>
  /// <'olives', '8'>
  /// ```
  final Map<String, String> ingredients;

  /// Steps to cook
  /// ```
  /// ['1. Cut greenbeans into 2 pieces each.', ..., '6. Put 2 spoons of olive oil.' ]
  /// ```
  final List<String> steps;

  final String stepsStr;

  /// An image of this recipe.
  final Image image;
  // Widget get image => Image.network(imageUrl);

  /*final*/ String imageUrl;

  Recipe({
    this.categoryName,
    this.code,
    @required this.name,
    this.ingredients,
    this.steps,
    this.stepsStr,
    this.image,
    this.imageUrl,
  });

  String ingredientsToString() {
    String result = '';
    if ((ingredients ?? {}).isEmpty) return '';
    ingredients.forEach((name, unit) {
      if (unit == null) {
        result += '$name, ';
      } else {
        result += '$name($unit), ';
      }
    });
    return result.substring(0, result.length - 2);
  }

  String stepsToString() {
    return steps
        .toString()
        .substring(1, steps.toString().length - 1)
        .replaceAll(', ', '\n');
    // return steps.map((step)=>'$step\n').toString();
  }

  Map<String, dynamic> toMap() {
    return {
      'category': categoryName,
      // 'code': code,
      'name': name,
      'ingredients': ingredients,
      'steps': stepsStr,
      'imageUrl': imageUrl,
    };
  }

  /// Returns [Recipe] object from [DocumentSnapshot] [documentSnapshot].
  static Recipe fromMap(dynamic documentSnapshot) {
    if (documentSnapshot['ingredients'] != null) {
      return Recipe(
        categoryName: documentSnapshot['category'],
        name: documentSnapshot['name'],
        stepsStr: documentSnapshot['steps'],
        imageUrl: documentSnapshot['imageUrl'],
        ingredients: Map.from(documentSnapshot['ingredients']),
      );
    }
    
    return Recipe(
      categoryName: documentSnapshot['category'],
      name: documentSnapshot['name'],
      stepsStr: documentSnapshot['steps'],
      imageUrl: documentSnapshot['imageUrl'],
    );
  }

  @override
  String toString() {
    return toMap().toString();
  }
}
