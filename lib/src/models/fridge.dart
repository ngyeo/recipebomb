// import 'package:flutter/material.dart';
import 'package:recipebomb/models.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Fridge {
  /// Find out the current item in the fridge
  /// and change the value
  /// Fridge.changeState(item)
  /// not using _selected.
  ///
  static Map<Ingredient, bool> ingredientsMap = {};

  /// Add each [items] to [ingredients] as a key with a value false.
  /// 
  /// TODO: Get value from Server.
  initialize(List<Ingredient> items) {
    ingredientsMap = {};
    items.forEach((item) => ingredientsMap.addAll({item: false}));
  }

  // Ingredient ingredient(String name) =>

  /// TODO: Check if this method can be run in multiple ingredients simultaneously.
  static Future<void> changeState(Ingredient item /*String itemName*/) async {
    // var item = ingredientsMap.keys.singleWhere((ingredient)=>ingredient.name == itemName);
    ingredientsMap[item] = !ingredientsMap[item];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(item.name, !prefs.getBool(item.name));
    //  NotifyValue; //?
  }

  /// Sets values of [ingredientsMap] as the latest data stored in SharedPreferences..
  ///
  /// CAUTION: All keys in ingredientsMap should be set before this method.
  Future<void> getAllState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    ingredientsMap.keys.forEach((ingrd) {
      if (!prefs.containsKey(ingrd.name)) prefs.setBool(ingrd.name, false);
      ingredientsMap[ingrd] = prefs.getBool(ingrd.name);
    });
  }

  /// SharedPreference

}
