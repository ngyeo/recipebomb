import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Ingredient {
  /// Document id in FireStore
  String id;
  String name;
  String type;

  Ingredient({
    this.id,
    @required this.name,
    this.type,
  });

  static Ingredient fromMap(DocumentSnapshot/*dynamic */ /*Map<String, String>*/ documentSnapshot) {
    return Ingredient(
      name: documentSnapshot['name'],
      type: documentSnapshot['type'],
      id: documentSnapshot.documentID,
    );
  }

  @override
  String toString() {
    return '$name($type)';
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'type': type,
    };
  }
}
