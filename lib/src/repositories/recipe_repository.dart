import 'dart:io';

import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:recipebomb/models.dart';

class RecipeRepository {
  /*Future<Recipe>*/
  Future<List<Recipe>> fetchRecipes() async {

    var mockup1 = Recipe(
      code: '1',
      name: 'Potato Greenbean Olive Mushroom',
      ingredients: {
        'potatoes': '2',
        'greenbeans': '100g',
        'olives': '8',
        'mushrooms': '3',
      },
      steps: [
        '1. adafgs',
        '2. sfdsfdsdsf',
        '3. adafgs',
      ],
      image: Image.asset('assets/images/20191128_200851.jpg'),
    );

    var mockup2 = Recipe(
      code: '2',
      name: 'Menu2',
      ingredients: {
        'potatoes': '2',
        'greenbeans': '100g',
        'olives': '8',
        'mushrooms': '3',
      },
      steps: [
        '1. adafgs',
        '2. sfdsfdsdsf',
        '3. adafgs',
      ],
      image: Image.asset('assets/images/20191216_184458.jpg'),
    );

    var mockup3 = Recipe(
      code: '3',
      name: 'Menu3',
      ingredients: {
        'potatoes': '2',
        'greenbeans': '100g',
        'olives': '8',
        'mushrooms': '3',
      },
      steps: [
        '1. adafgs',
        '2. sfdsfdsdsf',
        '3. adafgs',
      ],
      image: Image.asset('assets/images/20191217_235836.jpg'),
    );

    var mockup4 = Recipe(
      code: '4',
      name: 'Menu4',
      ingredients: {
        'potatoes': '2',
        'greenbeans': '100g',
        'olives': '8',
        'mushrooms': '3',
      },
      steps: [
        '1. adafgs',
        '2. sfdsfdsdsf',
        '3. adafgs',
      ],
      image: Image.asset('assets/images/20191129_190316.jpg'),
    );

    var mockup5 = Recipe(
      code: '5',
      name: 'Menu5',
      ingredients: {
        'potatoes': '2',
        'greenbeans': '100g',
        'olives': '8',
        'mushrooms': '3',
      },
      steps: [
        '1. adafgs',
        '2. sfdsfdsdsf',
        '3. adafgs',
      ],
      image: Image.asset('assets/images/20191218_234926.jpg'),
    );
    
    QuerySnapshot snapshot = await Firestore.instance.collection('recipes').getDocuments();
    List<Recipe> recipes = List<Recipe>.from(snapshot.documents.map<Recipe>((recipe) => Recipe.fromMap(recipe)));
    
    return recipes;
    // return [mockup1, mockup2, mockup3, mockup4, mockup5];
  }

  /// Inserts a [recipe] to Firebase.
  Future<void> insertARecipe({Recipe recipe, File image}) async {
    if (image != null) {
      final FirebaseStorage firebaseStorage = FirebaseStorage.instance;

      // Upload an image
      final imageCode = DateTime.now().toString();
      final StorageReference storageReference =
          firebaseStorage.ref().child('recipeImages/$imageCode');
      final StorageUploadTask storageUploadTask =
          storageReference.putFile(image);

      await storageUploadTask.onComplete;

      // Get an image url
      final imageUrl = await storageReference.getDownloadURL();
      recipe.imageUrl = imageUrl;
    }

    // print(await Firestore.instance.collection('recipes/${recipe.imageCode}').add(recipe.toMap()));
    await Firestore.instance.collection('recipes').add(recipe.toMap());

    print(recipe.toMap());
  }
}
