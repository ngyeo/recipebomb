
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:recipebomb/models.dart';

class IngredientRepository {
  /// Inserts an [Ingredient].
  Future<bool> insertAnIngredient({Ingredient ingredient}) async {
    if (ingredient == null) return false;

    final v = await Firestore.instance
        .collection('ingredient')
        .add(ingredient.toMap());
    return v.documentID != null;
  }

  Future<void> deleteAnIngredient({Ingredient ingredient}) async {
    await Firestore.instance.collection('ingredient').document(ingredient.id).delete();
  }
}
