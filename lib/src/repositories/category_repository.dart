import 'package:flutter/material.dart';

import 'package:recipebomb/models.dart';

class CategoryCon {
  static List<Category> categories;
  List<Category> fetchCategories() {
    var mockup1 = Category(name: 'Korean');
    var mockup2 = Category(name: 'Italian', color: Colors.yellow[300]);
    var mockup3 = Category(name: 'Spanish', color: Colors.green[300]);
    var mockup4 = Category(name: 'American', color: Colors.blueGrey[300]);
    var mockup5 = Category(name: 'Chinese', color: Colors.brown[300]);

    categories = [mockup1, mockup2, mockup3, mockup4, mockup5]; // is added later.

    return [mockup1, mockup2, mockup3, mockup4, mockup5];
  }
}
