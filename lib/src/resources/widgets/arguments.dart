import 'package:recipebomb/models.dart';

class Arguments{
  String categoryName;
  Recipe recipe;

  Arguments({
    this.categoryName,
    this.recipe,
  });
}