import 'package:flutter/material.dart';

/// Loader widget.
class Loader extends StatelessWidget {
  Loader({this.message = ''});

  final String message;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(message),
          Center(child: CircularProgressIndicator()),
        ],
      ),
    );
  }
}
