import 'package:flutter/material.dart';
import 'package:recipebomb/src/resources/services/globals.dart' as globals;

class CustomScaffold extends StatefulWidget {
  CustomScaffold({
    @required this.body,
    this.title = '',
    this.isHome = false,
    // int currentTabIndex,
    // Function onChange,
    // @required context,
  });
  final Widget body;
  final String title;
  final bool isHome;
  // final
  @override
  _CustomScaffoldState createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  // String _vegOption = globals.vegOption;

  @override
  Widget build(BuildContext context) {
    final drawer = Drawer(
      child: ListView(
        children: <Widget>[
          Text('Recipe Bomb', style: TextStyle(fontSize: 20)),
          ListTile(
            title: Text('Main page'),
            // onTap: () => Navigator.pushReplacementNamed(context, '/'),
            onTap: () =>
                Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false),
          ),
          ListTile(
            title: Text('My fridge'),
            // onTap: () => Navigator.pushReplacementNamed(context, '/myFridge'),
            onTap: () => Navigator.pushNamed(context, '/myFridge'),
          ),
          ListTile(
            title: Text('Add a recipe'),
            onTap: () => Navigator.pushNamed(context, '/addition'),
          ),
          SizedBox(
            height: 10.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(title: Text('Vegetarian option')),
              Column(
                children: ['X', 'Pesco', 'Lacto-ovo', 'Lacto', 'Ovo', 'Vegan']
                    .map(
                      (value) => ListTile(
                        leading: Radio<String>(
                          value: value,
                          groupValue: globals.vegOption,
                          onChanged: (String val) {
                            setState(() => globals.vegOption = val);
                          },
                        ),
                        title: Text(value),
                      ),
                    )
                    .toList(),
              ),
            ],
          ),
        ],
      ),
    );

    final bottomAppBar = BottomAppBar(
      color: Theme.of(context).primaryColor,
      child: Row(
        children: <Widget>[
          IconButton(
            icon: Icon(
              Icons.menu,
              color: Theme.of(context).buttonColor,
            ),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          )
        ],
      ),
    );

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: widget.isHome ? Text('Recipe Bomb') : Text(widget.title),
          leading: !widget.isHome ? BackButton() : Icon(Icons.grade),
          actions: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // Icon(Icons.library_books),
                Icon(Icons.import_contacts),
                Text('My Recipes'),
              ],
            )
          ],
        ),
        body: widget.body,
        drawer: drawer,
        bottomNavigationBar: bottomAppBar,
      ),
    );
  }
}

// Widget CustomScaffold(
//     {@required Widget body,
//     int currentTabIndex,
//     Function onChange,
//     @required context}) {
//   GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
// //    final _kBottomNavBarItems = <BottomNavigationBarItem>[
// //       BottomNavigationBarItem(
// //           icon: Icon(Icons.category), title: Text('Categories')),
// //       BottomNavigationBarItem(
// //           icon: Icon(Icons.check_circle), title: Text('Ingredients')),
// //       BottomNavigationBarItem(
// //           icon: Icon(Icons.cloud), title: Text('<Something>')),
// //     ];

// // final bottomNavBar = BottomNavigationBar(
// //       items: _kBottomNavBarItems,
// //       currentIndex: currentTabIndex,
// //       type: BottomNavigationBarType.fixed,
// //       onTap: (int index) {
// //         // setState(() {
// //         onChange(() {
// //           currentTabIndex = index;
// //         });
// //       },
// //     );

//   final drawer = Drawer(
//       child: ListView(
//         children: <Widget>[
//           Text('Recipe Bomb', style: TextStyle(fontSize: 20)),
//           ListTile(
//             title: Text('Main page'),
//             onTap: () => Navigator.pushNamed(context, '/'),
//           ),
//           ListTile(
//             title: Text('My fridge'),
//             onTap: () => Navigator.pushNamed(context, '/myFridge'),
//           ),
//         ],
//       ),
//     );

//   final bottomAppBar = BottomAppBar(
//     color: Theme.of(context).primaryColor,
//     child: Row(
//       children: <Widget>[
//         IconButton(
//             icon: Icon(
//               Icons.menu,
//               color: Theme.of(context).buttonColor,
//             ),
//             onPressed: ()=> _scaffoldKey.currentState.openDrawer(),
//         )
//       ],
//     ),
//   );
//   return Scaffold(
//     key: _scaffoldKey,
//     appBar: AppBar(
//       leading: BackButton(),
//       actions: <Widget>[
//         Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: <Widget>[
//             Icon(Icons.library_books),
//             Text('My Recipes'),
//           ],
//         )
//       ],
//     ),
//     body: body,
//     drawer: drawer,
//     bottomNavigationBar: bottomAppBar,
//   );
// }
