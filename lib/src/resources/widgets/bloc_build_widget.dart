import 'package:flutter/material.dart';
import 'package:recipebomb/blocs.dart';
import 'package:recipebomb/widgets.dart';

class BlocBuildWidget extends StatelessWidget {
  BlocBuildWidget({
    @required this.state,
  });
  // final BuildContext context;
  final BlocState state;

  @override
  Widget build(BuildContext context) {
    if (state is BlocLoading || state is BlocInit) {
      return Loader();
    } else {
      return Center(
          child: Column(
        children: <Widget>[
          Text((state as BlocError).error.toString()),
          CircularProgressIndicator(),
        ],
      ));
    }
  }
}
