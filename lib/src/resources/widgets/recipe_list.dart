import 'package:flutter/material.dart';

import 'package:recipebomb/models.dart';

class RecipeList extends StatelessWidget {
  final List<Recipe> recipes;

  RecipeList({@required this.recipes});

  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;

    _imageContainer(Image img, String menuName) {
      return Container(
        decoration:
            BoxDecoration(borderRadius: new BorderRadius.circular(200.0)),
        padding: const EdgeInsets.all(3.0),
        child: Stack(
          children: <Widget>[
            img,
            Container(
              alignment: Alignment.center, //
              child: Text(
                menuName,
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.black54,
            )
          ],
        ),
      );
    }

    _navigateTo(/*BuildContext context, */ Recipe recipe) {
      Navigator.of(context).pushNamed('/recipe', arguments: recipe);
    }

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
      itemCount: recipes.length,
      itemBuilder: (context, i) {
        return GestureDetector(
          child: _imageContainer(recipes[i].image, recipes[i].name),
          onTap: () => _navigateTo(recipes[i]),
        );
      },
    );
  }
}
