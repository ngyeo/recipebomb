import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';

class ImageBuilder extends StatelessWidget {
  ImageBuilder({
    @required this.imageUrl,
  });
  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Center(child: Icon(Icons.error)),
    );
  }
}
