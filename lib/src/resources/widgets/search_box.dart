import 'package:flutter/material.dart';

class SearchBox extends StatelessWidget {
  final String hintText;

  SearchBox({
    this.hintText,
  });
  
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        hintText: hintText,
      ),
      // onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),

      
    );
  }
}