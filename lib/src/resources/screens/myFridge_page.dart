import 'package:flutter/material.dart';
import 'package:recipebomb/main.dart';
import 'package:recipebomb/models.dart';
import 'package:recipebomb/src/repositories/ingredient_repository.dart';

import 'package:recipebomb/widgets.dart';

class MyFridgePage extends StatefulWidget {
  @override
  _MyFridgePageState createState() => _MyFridgePageState();
}

class _MyFridgePageState extends State<MyFridgePage> {
  List<Ingredient> _vegetables;
  List<Ingredient> _sauce;
  List<TextEditingController> _controllers;

  @override
  void initState() {
    super.initState();
    _vegetables = Fridge.ingredientsMap.keys
        .where((ingredient) => ingredient.type == 'Vegetable')
        .toList();
    _sauce = Fridge.ingredientsMap.keys
        .where((ingredient) => ingredient.type == 'Sauce')
        .toList();
    _controllers = [TextEditingController(), TextEditingController()];
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: ListView(
          children: [
            _section(title: 'Vegetables', items: _vegetables),
            _section(title: 'Sauce', items: _sauce),
          ],
        ),
      ),
    );
  }

  Future<void> _setState() async {
    await BootApp().setIngredients();
    _vegetables = Fridge.ingredientsMap.keys
        .where((ingredient) => ingredient.type == 'Vegetable')
        .toList();
    _sauce = Fridge.ingredientsMap.keys
        .where((ingredient) => ingredient.type == 'Sauce')
        .toList();
    setState(() {});
  }

  _section({String title, @required List<Ingredient> items}) {
    final int i = (title == 'Vegetables') ? 0 : 1;
    return Column(
      children: [
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(title ?? ''),
              // Expanded(),
              IconButton(
                icon: Icon(Icons.add),
                onPressed: () {},
              ),
            ]),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0),
          child: TextField(
            controller: _controllers[i],
            onSubmitted: (str) async {
              final success = await IngredientRepository().insertAnIngredient(
                  ingredient: Ingredient(
                      name: _controllers[i].text, type: items.first.type));

              if (success) {
                await _setState();
                _controllers[i].text = '';
              }
            },
          ),
        ),
        Wrap(
          alignment: WrapAlignment.center,
          spacing: 8.0,
          runSpacing: 2.0,
          direction: Axis.horizontal,
          children: items
              .map(
                (Ingredient item) => GestureDetector(
                  onVerticalDragEnd: (_) async {
                    await IngredientRepository()
                        .deleteAnIngredient(ingredient: item);
                    await _setState();
                  },
                  child: ChoiceChip(
                    label: Text(item.name),
                    selected: Fridge.ingredientsMap[item],
                    onSelected: (value) {
                      Fridge.changeState(item);
                      setState(() {});
                    },
                    selectedColor: Theme.of(context).primaryColor.withAlpha(170),
                    labelStyle: TextStyle(color: Colors.black),
                  ),
                ),
              )
              .toList(),
        ),
      ],
    );
  }
}
