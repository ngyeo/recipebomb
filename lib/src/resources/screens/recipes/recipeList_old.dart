// import 'package:flutter/material.dart';

// import 'package:recipebomb/models.dart';
// import 'package:recipebomb/repositories.dart';

// /// Show lists for each category
// /// BottomTabBar - [TabBarView]
// class RecipeListPage extends StatefulWidget {
//   /// Filter to filter recipes
//   /// 
//   /// can be Category(Korean, Spanish, Italian, etc), Vegetarian, etc.
//   final dynamic filter;

//   RecipeListPage(this.filter);
//   @override
//   _RecipeListPageState createState() => _RecipeListPageState();
// }

// class _RecipeListPageState extends State<RecipeListPage>/* with TickerProviderStateMixin */{
//   int _currentTabIndex = 0;

//   List<Recipe> recipes = [];
//   bool _isLoaded = false;

//   @override
//   void didChangeDependencies() {
//     super.didChangeDependencies();

//     /// Get data from API
//     /// TODO: Reorganize
//     if (!_isLoaded) {
//       recipes.addAll(RecipeRepository().fetchRecipes());
//       _isLoaded = true;
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     final _kBottomNavBarItems = <BottomNavigationBarItem>[
//       BottomNavigationBarItem(
//           icon: Icon(Icons.category), title: Text('Categories')),
//       BottomNavigationBarItem(
//           icon: Icon(Icons.check_circle), title: Text('Ingredients')),
//       BottomNavigationBarItem(
//           icon: Icon(Icons.cloud), title: Text('<Something>')),
//     ];

//     final bottomNavBar = BottomNavigationBar(
//       items: _kBottomNavBarItems,
//       currentIndex: _currentTabIndex,
//       type: BottomNavigationBarType.fixed,
//       onTap: (int index) {
//         setState(() {
//           _currentTabIndex = index;
//         });
//       },
//     );

//     return Scaffold(
//       appBar: AppBar(
//         // leading: Icon(Icons.arrow_back),
//         leading: BackButton(),
//         actions: <Widget>[
//           Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: <Widget>[
//               Icon(Icons.library_books),
//               Text('My Recipes'),
//             ],
//           )
//         ],
//       ),
//       body: _kTabPages(_currentTabIndex),
//       bottomNavigationBar: bottomNavBar,
//     );
//   }

//   _kTabPages(int currentTabIndex) {
//     var list = <Widget>[
//       Center(
//           child: _gridList(recipes)), // _gridList should be static or change this one.
//       Center(
//         child: Text('B'),
//       ),
//       Center(
//         child: Text('C'),
//       ),
//     ];
//     return list[currentTabIndex];
//   }

//   _gridList(List<Recipe> recipes) {
//     final orientation = MediaQuery.of(context).orientation;

//     return GridView.builder(
//       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//           crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
//       itemCount: recipes.length,
//       itemBuilder: (context, i) {
//         return GestureDetector(
//           child: _imageContainer(recipes[i].image, recipes[i].name),
//           onTap: () => _navigateTo(recipes[i]),
//         );
//       },
//     );
//     // return GridView.count(
//     //   crossAxisCount: 2,
//     //   scrollDirection: Axis.vertical,
//     //   children: <Widget>[
//     //     GestureDetector(
//     //       child: _imageContainer('assets/images/20191128_200851.jpg', 'menu1'),
//     //       onTap: () => _navigateTo(Recipe(code: null, name: null)),
//     //     ),
//     //     // onTap: _navigateTo('somewhere'),),
//     //     _imageContainer('assets/images/20191216_184458.jpg', 'menu2'),
//     //     _imageContainer('assets/images/20191217_235836.jpg', 'menu3'),
//     //     _imageContainer('assets/images/20191129_190316.jpg', 'menu4'),
//     //     _imageContainer('assets/images/20191218_234926.jpg', 'menu5'),
//     //   ],
//     // );
//   }

//   _imageContainer(Image img, String menuName) {
//     return Container(
//       decoration: BoxDecoration(borderRadius: new BorderRadius.circular(200.0)),
//       padding: const EdgeInsets.all(3.0),
//       child: Stack(
//         children: <Widget>[
//           img,
//           Container(
//             alignment: Alignment.center, //
//             child: Text(
//               menuName,
//               style: TextStyle(color: Colors.white),
//             ),
//             color: Colors.black54,
//           )
//         ],
//       ),
//     );
//   }

//   _imageContainer_old(String imageDirectory, String menuName) {
//     return Container(
//       decoration: BoxDecoration(borderRadius: new BorderRadius.circular(200.0)),
//       padding: const EdgeInsets.all(3.0),
//       child: Stack(
//         children: <Widget>[
//           Image(image: AssetImage(imageDirectory)),
//           Container(
//             alignment: Alignment.center, //
//             child: Text(
//               menuName,
//               style: TextStyle(color: Colors.white),
//             ),
//             color: Colors.black54,
//           )
//         ],
//       ),
//     );
//   }

//   _navigateTo(/*BuildContext context, */ Recipe recipe) {
//     Navigator.of(context).pushNamed('/recipe', arguments: recipe);
//   }

  

  
// }
