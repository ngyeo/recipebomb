import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:recipebomb/blocs.dart';

import 'package:recipebomb/models.dart';
import 'package:recipebomb/repositories.dart';
import 'package:recipebomb/widgets.dart';

class AdditionPage extends StatefulWidget {
  @override
  _AdditionPageState createState() => _AdditionPageState();
}

class _AdditionPageState extends State<AdditionPage> {
  int ingredientsNum = 3;
  List<ListTile> ingredientTiles = [];
  TextEditingController _titleController;
  List<Map<String, TextEditingController>> _ingredientControllers;
  TextEditingController _stepsController;
  List<DropdownMenuItem<String>> categories;
  String _category;
  File _image;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _ingredientControllers = [
      {
        'title': TextEditingController(),
        'unit': TextEditingController(),
      },
    ];
    _stepsController = TextEditingController();
    ingredientTiles.add(_ingredientTile(_ingredientControllers.last));

    categories = CategoryCon.categories
        .map((category) => DropdownMenuItem(
              value: category.name,
              child: Text(category.name),
            ))
        .toList();
  }

  @override
  void dispose() {
    super.dispose();
    _titleController.dispose();
    _ingredientControllers.forEach((c) {
      c['title'].dispose();
      c['unit'].dispose();
    });
    _stepsController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // BlocBuilder(builder: (context, state) {
    //   if (state is RecipeAdding) {
    //     CircularProgressIndicator();
    //   } else if (state is RecipeAdded) {
    //     showDialog(
    //       context: context,
    //       child: AlertDialog(
    //         title: Text('Success'),
    //         actions: <Widget>[
    //           FlatButton(
    //             child: Text('Close'),
    //             onPressed: () {
    //               Navigator.pop(context);
    //               Navigator.pop(context);
    //             },
    //           ),
    //         ],
    //       ),
    //     );
    //   } else {
    //     // return Text('error'); //
    //   }
    // });
    return CustomScaffold(
      title: 'Add a recipe',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
        child: ListView(
          children: <Widget>[
            Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  child: DropdownButton(
                    hint: Text('Category'),
                    value: _category,
                    items: categories,
                    onChanged: (value) {
                      setState(() => _category = value);
                    },
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Text('Title'),
            ),
            Container(
              child: TextFormField(
                controller: _titleController,
                decoration: InputDecoration(
                  hintText: 'Title',
                  border: OutlineInputBorder(),
                ),
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Text('Ingredients'),
            ),
            _ingredients(),
            Divider(),
            Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Text('Steps'),
            ),
            TextFormField(
              controller: _stepsController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: '1. (First step)\n2. (Second step)',
              ),
              keyboardType: TextInputType.multiline,
              maxLines: null,
            ),
            Divider(),
            _image == null
                ? FlatButton(
                    child: Text('Select an image.'),
                    onPressed: getImage,
                  )
                : Image.file(_image),
            RaisedButton(
              child: BlocBuilder<RecipeBloc, BlocState>(
                builder: (context, state) {
                  if (state is RecipeAdding) {
                    return CircularProgressIndicator();
                  } else if (state is RecipeAdded) {
                    return Icon(Icons.check, color: Colors.white);
                  } else if (state is RecipeError) {
                    return Text(state.error.toString());
                  } else {//(state is RecipeInit){
                    return Text('Submit',
                        style: TextStyle(color: Colors.white));
                  }
                },
              ),
              onPressed: _submit,
              color: Theme.of(context).primaryColor,
            ),
          ],
        ),
      ),
    );
  }

  /// TODO: Set an auth token for request.
  Future getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if(image == null) return;
    
    setState(() {
      _image = image;
    });
  }

  _ingredients() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: ingredientTiles.length + 1,
      itemBuilder: (context, i) {
        if (i == ingredientTiles.length) {
          return IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              _addIngredient();
              setState(() {});
            },
          );
        }
        return ingredientTiles[i];
      },
    );
  }

  ListTile _ingredientTile(Map<String, TextEditingController> controller) {
    return ListTile(
      // contentPadding: EdgeInsets.symmetric(horizontal: -1),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // Expanded(child: SearchBox(hintText: 'Ingredient',)),
          Expanded(
            child: TextFormField(
              controller: controller['title'],
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Ingredient',
              ),
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            ),
          ),
          SizedBox(width: 10.0),
          SizedBox(
            width: 120.0,
            child: TextFormField(
              controller: controller['unit'],
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Unit',
              ),
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            ),
          ),
        ],
      ),
      trailing: IconButton(
        icon: Icon(Icons.cancel),
        onPressed: () {
          int i = _ingredientControllers.indexOf(controller);
          _deleteIngredientAt(i);
          setState(() {});
        },
      ),
    );
  }

  _addIngredient() {
    _ingredientControllers.add({
      'title': TextEditingController(),
      'unit': TextEditingController(),
    });
    ingredientTiles.add(_ingredientTile(_ingredientControllers.last));
  }

  _deleteIngredientAt(int index) {
    _ingredientControllers.removeAt(index);
    ingredientTiles.removeAt(index);
  }

  void _submit() {
    Map<String, String> map = {};
    _ingredientControllers.forEach((controller) =>
        map.addAll({controller['title'].text: controller['unit'].text}));
    
    var recipe = Recipe(
      categoryName: _category,
      name: _titleController.text,
      ingredients: map,
      stepsStr: _stepsController.text.trim(),
    );
    
    BlocProvider.of<RecipeBloc>(context).add(AddARecipe(recipe: recipe, image: _image));
  }
}
