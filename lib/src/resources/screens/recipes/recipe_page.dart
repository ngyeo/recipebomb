import 'package:flutter/material.dart';

import 'package:recipebomb/models.dart';
import 'package:recipebomb/widgets.dart';

/// 1 RecipePage. Decide where to pass a variable.
class RecipePage extends StatelessWidget {
  final Recipe recipe;
  final String categoryName;

  RecipePage({@required this.categoryName, @required this.recipe});

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      title: categoryName,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(5.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Hero(
                tag: recipe.name,
                // placeholderBuilder: (context, size, widget) {
                //   return Center(
                //     child: CircularProgressIndicator(),
                //   );
                // },
                child: ImageBuilder(imageUrl: recipe.imageUrl),
              ),
              SizedBox(height: 5.0),
              Text(
                recipe.name,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10.0),
              Text('Ingredients: ${recipe.ingredientsToString()}'),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                child: Text(recipe.stepsStr ?? recipe.stepsToString()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
