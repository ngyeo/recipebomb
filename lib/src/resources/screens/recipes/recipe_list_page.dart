import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:recipebomb/blocs.dart';
import 'package:recipebomb/models.dart';
import 'package:recipebomb/widgets.dart';

/// Show lists for each category
/// BottomTabBar - [TabBarView]
class RecipeListPage extends StatefulWidget {
  /// A filter to filter recipes
  ///
  /// can be Category(Korean, Spanish, Italian, etc), Vegetarian, etc.
  /// Not String type so far.
  final dynamic filter;

  RecipeListPage(this.filter);
  @override
  _RecipeListPageState createState() => _RecipeListPageState();
}

class _RecipeListPageState extends State<RecipeListPage> {
  String _selectedMode = modes[0];
  static const List<String> modes = ['Only in fridge', 'All'];
  List<DropdownMenuItem> _dropDownMenuModes = modes
      .map((mode) => DropdownMenuItem(child: Text(mode), value: mode))
      .toList();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<RecipeBloc>(context).add(LoadRecipes());
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      title: widget.filter.name,
      body: BlocBuilder<RecipeBloc, BlocState>(
        builder: (context, state) {
          if (state is RecipesLoaded) {
            return Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 10.0),
                  alignment: Alignment.centerRight,
                  child: DropdownButton<String>(
                    items: _dropDownMenuModes,
                    value: _selectedMode,
                    onChanged: (value) => setState(() => _selectedMode = value),
                  ),
                ),
                Expanded(child: _gridList(state.recipes)),
                // _gridList should be static or change this one.),
              ],
            );
          } else {
            return BlocBuildWidget(state: state);
          }
        },
      ),
    );
  }

  _gridList(List<Recipe> recipes) {
    final orientation = MediaQuery.of(context).orientation;

    return GridView.builder(
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
      itemCount: recipes.length,
      itemBuilder: (context, i) {
        return GestureDetector(
          child: _imageContainer(recipes[i].imageUrl, recipes[i].name),
          onTap: () => _navigateTo(recipes[i]),
        );
      },
    );
  }

  _imageContainer(String imgUrl, String menuName) {
    return Container(
      decoration: BoxDecoration(borderRadius: new BorderRadius.circular(200.0)),
      padding: const EdgeInsets.all(3.0),
      child: Stack(
        children: <Widget>[
          Hero(
            tag: menuName,
            //   placeholderBuilder: (context, size, widget) {
            //     return Center(
            //       child: CircularProgressIndicator(),
            //     );
            //   },
            child: ImageBuilder(imageUrl: imgUrl),
          ),
          Container(
            alignment: Alignment.center, //
            child: Text(
              menuName,
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.black54,
          )
        ],
      ),
    );
  }

  _navigateTo(Recipe recipe) {
    Navigator.of(context).pushNamed('/recipe',
        arguments: Arguments(categoryName: widget.filter.name, recipe: recipe));
  }
}
