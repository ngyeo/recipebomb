import 'package:flutter/material.dart';

import 'package:recipebomb/widgets.dart';
import 'package:recipebomb/models.dart';
import 'package:recipebomb/screens.dart';
import 'package:recipebomb/repositories.dart';

/// Show lists for each category
/// BottomTabBar - [TabBarView]
class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {

  List<Category> categories = [];
  bool _isLoaded = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    /// Get data from API
    if (!_isLoaded) {
      categories.addAll(CategoryCon().fetchCategories());
      _isLoaded = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      isHome: true,
      body: Center(child: _gridList(categories)),
    );
  }

  _gridList(List<Category> categories) {
    final orientation = MediaQuery.of(context).orientation;

    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3),
      itemCount: categories.length,
      itemBuilder: (context, i) {
        return GestureDetector(
          child: _container(categories[i]),
          onTap: () => _navigateTo(categories[i]),
        );
      },
    );
  }

  _container(Category category) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: new BorderRadius.circular(50.0), color: category.color),
      padding: const EdgeInsets.all(3.0),
      alignment: Alignment.center,
      child: Text(
        category.name,
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  _navigateTo(Category category) {
    Navigator.of(context).pushNamed('/recipeList', arguments: category);
  }
}
