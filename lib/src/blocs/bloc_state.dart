

abstract class BlocState /*extends Equatable*/{
  const BlocState();
}

class BlocInit extends BlocState{}

class BlocLoading extends BlocState{}

class BlocLoaded extends BlocState{}

class BlocError extends BlocState{
  final Object error;

  BlocError({this.error});
}