

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:recipebomb/models.dart';

abstract class RecipeEvent {
  const RecipeEvent();
}

class AddARecipe extends RecipeEvent{
  final Recipe recipe;
  
  /// An image file from ImagePicker.
  /// 
  /// Can be null.
  final File image;

  AddARecipe({@required this.recipe, this.image}): assert(recipe != null);
}

/// Currently, load all recipes.
class LoadRecipes extends RecipeEvent{
  // final filter;
  // final BuildContext context; // TODO: Delete

  // LoadRecipes({@required this.context}): assert(context != null);
}