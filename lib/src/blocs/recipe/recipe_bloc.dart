import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recipebomb/blocs.dart';
import 'package:recipebomb/repositories.dart';

class RecipeBloc extends Bloc<RecipeEvent, BlocState> {
  final RecipeRepository recipeRepository;

  RecipeBloc({@required this.recipeRepository});

  @override
  BlocState get initialState => /*throw */RecipeInit(); // ?

  @override
  Stream<BlocState> mapEventToState(RecipeEvent event) async* {
    if (event is AddARecipe) {
      yield RecipeAdding();

      try {
        await recipeRepository.insertARecipe(recipe: event.recipe, image: event.image);
        yield RecipeAdded();
      } catch (e) {
        yield RecipeError(error: e);
      }
    }

    if (event is LoadRecipes) {
      yield RecipesLoading();

      try{
        final recipes = await recipeRepository.fetchRecipes();
        yield RecipesLoaded(recipes: recipes);
      } catch (e) {
        yield RecipeError(error: e);
      }
    }
    // throw UnimplementedError();
  }
}
