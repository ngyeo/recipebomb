import 'package:flutter/material.dart';

import 'package:recipebomb/blocs.dart';
import 'package:recipebomb/models.dart';

class RecipeInit extends BlocInit{}//

class RecipeAdding extends BlocLoading{}

class RecipesLoading extends BlocLoading{}

class RecipesLoaded extends BlocLoaded{
  final List<Recipe> recipes;

  RecipesLoaded({@required this.recipes}):
  assert(recipes != null);
}

class RecipeAdded extends BlocLoaded{}

class RecipeError extends BlocError{
  @override
  final Object error;

  RecipeError({this.error});
}