/// Only mock-up data.
/// 
/// Delete someday

import 'package:flutter/material.dart';
import 'package:recipebomb/models.dart';

var recipeMockup1 = Recipe(
  code: '1',
  name: 'Potato Greenbean Olive Mushroom',
  ingredients: {
    'potatoes': '2',
    'greenbeans': '100g',
    'olives': '8',
    'mushrooms': '3',
  },
  steps: [
    '1. adafgs',
    '2. sfdsfdsdsf',
    '3. adafgs',
  ],
  image: Image.asset('assets/images/20191128_200851.jpg'),
);

var recipeMockup2 = Recipe(
  code: '2',
  name: 'Menu2',
  ingredients: {
    'potatoes': '2',
    'greenbeans': '100g',
    'olives': '8',
    'mushrooms': '3',
  },
  steps: [
    '1. adafgs',
    '2. sfdsfdsdsf',
    '3. adafgs',
  ],
  image: Image.asset('assets/images/20191216_184458.jpg'),
);

var recipeMockup3 = Recipe(
  code: '3',
  name: 'Menu3',
  ingredients: {
    'potatoes': '2',
    'greenbeans': '100g',
    'olives': '8',
    'mushrooms': '3',
  },
  steps: [
    '1. adafgs',
    '2. sfdsfdsdsf',
    '3. adafgs',
  ],
  image: Image.asset('assets/images/20191217_235836.jpg'),
);

var recipeMockup4 = Recipe(
  code: '4',
  name: 'Menu4',
  ingredients: {
    'potatoes': '2',
    'greenbeans': '100g',
    'olives': '8',
    'mushrooms': '3',
  },
  steps: [
    '1. adafgs',
    '2. sfdsfdsdsf',
    '3. adafgs',
  ],
  image: Image.asset('assets/images/20191129_190316.jpg'),
);

var recipeMockup5 = Recipe(
  code: '5',
  name: 'Menu5',
  ingredients: {
    'potatoes': '2',
    'greenbeans': '100g',
    'olives': '8',
    'mushrooms': '3',
  },
  steps: [
    '1. adafgs',
    '2. sfdsfdsdsf',
    '3. adafgs',
  ],
  image: Image.asset('assets/images/20191218_234926.jpg'),
);
